import sqlite3

db = sqlite3.connect('database.db')
cursor = db.cursor()

def get_flavours():
    flavour_dict = {}
    message = ""
    data = cursor.execute('SELECT flavour FROM config').fetchall()
    flavour = [x[0] for x in data]
    for element in flavour:
        message += str(element) + ','
    message = message[:-1]
    flavours = message.split(',')
    for x in range(0,len(flavours)):
        flavour_dict[x+1] = flavours[x]
    return flavour_dict

flavours = get_flavours()

print(flavours)