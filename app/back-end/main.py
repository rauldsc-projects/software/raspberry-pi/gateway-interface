from flask import Flask, request, render_template
import sqlite3, requests, json

app = Flask(__name__)

db = sqlite3.connect('database.db',check_same_thread=False)
cursor = db.cursor()

def get_flavours():
    flavour_dict = {}
    message = ""
    data = cursor.execute('SELECT flavour FROM config').fetchall()
    flavour = [x[0] for x in data]
    for element in flavour:
        message += str(element) + ','
    message = message[:-1]
    flavours = message.split(',')
    for x in range(0,len(flavours)):
        flavour_dict[x+1] = flavours[x]
    return flavour_dict
    
    
@app.route('/')
def home():
    return render_template('index.html')

@app.route('/flavour/', methods=['GET','POST'])
def post_flavours():
    message = json.dumps(get_flavours(), ensure_ascii=False)
    return message

app.run(debug = True)
